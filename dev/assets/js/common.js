(function ($) {
	var config = function () {
		//ポップアップリンクに置換
		$(".commonPop").easyPop();
		//アンカーリンクをスムージング
		$('a[href^="#"]').smoothScroll();
		var _ua = Fourdigit.set()._ua,
			_browser = Fourdigit.set()._browser,
			_breakP = Fourdigit.set()._breakP,
			_winSize = Fourdigit.set()._winSize;
		// ブラウザを判別し、bodyにそのブラウザ名のクラスを付与
		for (var key in _browser) {
			if (_browser.hasOwnProperty(key)) {
				if (_browser[key] == true) {
					$('body').addClass(key);
				}
			}
		}
	}

	$(function () {
		/**
		 * 共通系処理
		 * @description
		 * サイト共通で機能させる処理はここに書きます。
		 */
		config();
		/**
		 * VIEWPORT 切り替え
		 * @description
		 * TBとSPの場合で、それぞれviewportの値を切り替えます。
		 */
		function updateMetaViewport() {
			var viewportContent;
			if (_ua.SP) {
				viewportContent = "width=device-width,initial-scale=1.0,maximum-scale=1.5,user-scalable=yes";
			} else if (_ua.TB) {
				viewportContent = "width=1200px";
			}
			document.querySelector("meta[name='viewport']").setAttribute("content", viewportContent);
		}
		if (_ua.SP || _ua.TB) {
			window.addEventListener("load", updateMetaViewport, false);
		}
		//android tel設定
		$("a[href^=tel]").click(function () {
			location.href = $(this).attr("href");
			return false;
		});

		// sp android クラス付与
		if (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) {
			$('body').addClass('android');
		}



		/**
		 * PAGE TOP
		 * @description
		 */
		var $topBtn = $('.pagetop');
		$topBtn.on('click', function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500, "linear");
			return false;
		});
		/**
		 * SP NAVI
		 * @description
		 */
		var $spMenu = $('#spMenu'),
			$gNav = $('#gNav'),
			spNavFlag = false;

		if (_breakP.SP) {
			$spMenu.on('click', function () {
				if (spNavFlag) {
					$spMenu.removeClass('spMenu-active');
					$gNav.fadeOut().removeClass('gNav-active');
					spNavFlag = false;
				} else {
					$spMenu.addClass('spMenu-active');
					$gNav.fadeIn().addClass('gNav-active');
					spNavFlag = true;
				}
			});
		}

		/**
		 * login page(cookie version)
		 * md5.jsを使って暗号化はしているものの簡易的なもののため、個人情報等重要な情報を扱うページには利用しないこと
		 * @description
		 * 現在loginページの処理をlogin page(localStorage version)の方を適用しています。
		 * login page(localStorage version)を使用する時は(cookie version)を削除して使用してください
		 */

		// アタリ用のコードです。login page(cookie version)を使用する時はlogin page(localStorage version)以下の範囲を削除してください
		if ($('#page').length > 99) {
			//ここまで

			var currentPage = $('.wrapper').attr('id'),
				currentPath = window.location.href.indexOf("limited");
			if (currentPath != -1) {
				var pw = {};
				var inputData;

				pw.init = function () {
					// クッキーを生成
					var cookie = '',
						allCookies = document.cookie,
						// cookieNameの値 'hogeLogin' は必ず物件ごとにユニークな値に変更してください
						cookieName = 'hogeLogin' + '=',
						cookiePosition = allCookies.indexOf(cookieName);

					if (cookiePosition > -1) {
						var startIndex = cookiePosition + cookieName.length,
							endIndex = allCookies.indexOf(';', startIndex);
						if (endIndex == -1) endIndex = allCookies.length;
						cookie = decodeURIComponent(allCookies.substring(startIndex, endIndex));
					}

					if (currentPage == 'limitedLogin') {
						if (cookie != 'logined') {
							// 限定ログインページ初来訪時の処理
							$('#submit').bind('click touchstart', function () {
								inputData = $('#password').val();
								pw.auth(inputData);
								// パスワードが間違ってた時にリロードしてしまう処理を無効化
								return false;
							});
						} else {
							// 限定ログインページパスワード認証済みの処理
							document.cookie = 'hogeLogin=logined; max-age=21600';
							location.href = 'index.html';
						}
					} else {
						// 限定ページに直接遷移した時にクッキーを認証するための処理
						loginCheck(cookie);
					}
				};

				pw.auth = function (value) {
					// md5の暗号を解読するための処理
					var str = CybozuLabs.MD5.calc(value);
					// アタリ用パスワード（本番用パスワードを追加するタイミングでコメントアウトを削除してください）
					// Password: atari
					if (str == '84af165428d59ddc126f456a5035e379') {
						document.cookie = 'hogeLogin=logined; max-age=21600';
						location.href = 'index.html';
					} else {
						alert('パスワードが間違っています');
					}
				}

				loginCheck = function (cookie) {
					if (cookie != 'logined') {
						location.href = 'login.html';
					}
				}
				// パスワード処理初期化
				pw.init();
			}

			// アタリ用のコードです。login page(cookie version)を使用する時はlogin page(localStorage version)以下の範囲を削除してください
		}
		//ここまで



		/**
		 * login page(localStorage version)
		 * md5.jsを使って暗号化はしているものの簡易的なもののため、個人情報等重要な情報を扱うページには利用しないこと
		 * @description
		 * 検証でlocalStorageのキーをクリアした時はlocalStorage.clear();の関数を適時使用していください
		 * login page(cookie version)を使用する時は(localStorage version)を削除して使用してください
		 */
		var currentPage = $('.wrapper').attr('id'),
			currentPath = window.location.href.indexOf("limited");
		if (currentPath != -1) {
			var pw = {};
			var inputData;

			pw.init = function () {
				// localStorageのキーの定義
				var limitedKey = window.localStorage.getItem('accessData');
				if (currentPage == 'limitedLogin') {
					if (limitedKey != 'logined') {
						// 限定ログインページ初来訪時の処理
						$('#submit').bind('click touchstart', function () {
							inputData = $('#password').val();
							pw.auth(inputData);
							// パスワードが間違ってた時にリロードしてしまう処理を無効化
							return false;
						});
					} else {
						// 限定ログインページパスワード認証済みの処理
						window.localStorage.setItem('accessData', 'logined');
						location.href = 'index.html';
					}
				} else {
					// 限定ページに直接遷移した時にクッキーを認証するための処理
					loginCheck(limitedKey);
				}
			};

			pw.auth = function (value) {
				// md5の暗号を解読するための処理
				var str = CybozuLabs.MD5.calc(value);
				// アタリ用パスワード（本番用パスワードを追加するタイミングでコメントアウトを削除してください）
				// Password: atari
				if (str == '84af165428d59ddc126f456a5035e379') {
					window.localStorage.setItem('accessData', 'logined');
					location.href = 'index.html';
				} else {
					alert('パスワードが間違っています');
				}
			}

			loginCheck = function (limitedKey) {
				if (limitedKey != 'logined') {
					location.href = 'login.html';
				}
			}
			// パスワード処理初期化
			pw.init();
		}

		// タイプライタ aos設定
		function TypeWriterAos() {}
		TypeWriterAos.prototype.func = function () {
			$(window).on('load resize', function () {
				var wh = $(window).height();
				AOS.init({
					// Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
					offset: wh / 4, // offset (in px) from the original trigger point
					duration: 600, // values from 0 to 3000, with step 50ms
					easing: 'ease', // default easing for AOS animations
					once: true, // whether animation should happen only once - while scrolling down
					mirror: false, // whether elements should animate out while scrolling past them
					anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
				});
			});
		}

		/*
		 * textAnimation(Split)
		 * */
		function TextAnimation() {}
		TextAnimation.prototype.func = function () {
			spanSplit('[data-aos="typeWriter"]', 'type');
			/*
			 * 第1引数：対象要素
			 * 第2引数：spanのclass名
			 * */
			function spanSplit(el, className) {
				/*要素の指定*/
				var $el = $(el);
				$el.each(function () {
					var el = this;
					/*指定した要素からnodeを取り出し*/
					$(el).contents().each(function (_, node) {
						/*nodeを一旦削除*/
						node.parentNode.removeChild(node);
						switch (node.nodeType) {
							/*nodeがテキストだった場合*/
							case Node.TEXT_NODE:
								/*テキストを分割*/
								var text_split = splitText(node);
								/*スパンで囲む*/
								$.each(text_split, function () {
									var random = Math.floor(Math.random() * (5 - 1) + 1);
									switch (random) {
										case 1:
											transClass = 'type-right';
											break;
										case 2:
											transClass = 'type-left';
											break;
										case 3:
											transClass = 'type-top';
											break;
										case 4:
											transClass = 'type-bottom';
											break;
									}
									$(el).append('<span class="' + className + ' ' + transClass + '">' + this + '</span>');
								});
								break;
							default:
								/*spanの中身にも適用*/
								if (node.childNodes.length > 0) {
									/*テキストを分割*/
									var text_split = splitText(node);
									var e = '';
									/*タグを分割するために中身を,*/
									node.innerHTML = ',';
									/*wrapのためにタグを分割*/
									var wrap = node.outerHTML.split(',');
									/*スパンで囲む*/
									$.each(text_split, function () {
										var random = Math.floor(Math.random() * (5 - 1) + 1);
										switch (random) {
											case 1:
												transClass = 'type-right';
												break;
											case 2:
												transClass = 'type-left';
												break;
											case 3:
												transClass = 'type-top';
												break;
											case 4:
												transClass = 'type-bottom';
												break;
										}
										e += ('<span class="' + className + ' ' + transClass + '">' + this + '</span>');
									});
									/*タグをまとめて囲む*/
									e = (wrap[0] + e + wrap[1]);
									$(el).append(e);
								} else {
									/*nodeはそのまま*/
									$(el).append(node);
								}
						}
					});
				});

				function splitText(node) {
					/*テキストからスペースを削除*/
					var text = node.textContent.trim();
					/*一文字ずつスプリット*/
					var text_split = text.split('');

					return text_split;
				}
			}
		}

		// 下層メインビジュアル、コンテンツエリア設定
		function LowMainVisual() {}
		LowMainVisual.prototype.func = function () {
			var scrollEffect = function () {
				var $sectionBlock = $('.section__title-block');

				var init = function () {
						bindEvent();
					},

					prepareSectionTitle = function () {
						var $list = document.querySelectorAll('.section__title-block'),
							$argArr = [].slice.call($list),
							$flagArr = [];

						for (var i = 0; i < $argArr.length; i++) {
							$flagArr.push(false);
						}

						$sectionBlock.each(function (i) {
							var $this = $(this);
							if ($this.find('.section__title').hasClass('aos-animate') && $flagArr[i] === false) {
								$flagArr[i] = true;
								$this.find('.section__visual').addClass('fadein');
							}
						});
					},

					bindEvent = function () {
						$(window).on('load scroll', function () {
							if ($('.page-title').hasClass('aos-animate')) {
								$('.low-main-visual__image').addClass('fadein');
							}
							prepareSectionTitle();
						});
					};

				return {
					init: init
				}
			}

			scrollEffect().init();
		}

		/**
		 * 各ページ固有の処理
		 * 基本的にローカルにJSは置かずにcommon内で完結させる。
		 */

		$(function () {
			var w = $(window).width();
			var x = 760;
			if (w <= x) {
				window.onorientationchange = function () {
					switch (window.orientation) {
						case 0:
							break;
						case 90:
							alert('※当サイトを最適な状態でご覧いただくには、スマートフォンを縦にした状態でご覧ください。');
							break;
						case -90:
							alert('※当サイトを最適な状態でご覧いただくには、スマートフォンを縦にした状態でご覧ください。');
							break;
					}
				};
			}
		});


		// スマホ時スクロールの制御
		function scroll_control(event) {
			event.preventDefault();
		}

		function no_scroll() {
			$('html,body').animate({ scrollTop: 0 }, '1');
			document.addEventListener("touchmove", scroll_control, {
				passive: false
			});
		}

		function return_scroll() {
			document.removeEventListener('touchmove', scroll_control, {
				passive: false
			});
		}

		switch ($('.wrapper').attr('id')) {
			case 'index':
				var wW = $(window).width();
				var tl_pc = new TimelineMax();
				var tl_sp = new TimelineMax();
				var tl = new TimelineMax();
				var tl2 = new TimelineMax();
				$('.opening-anime__skip-btn').on('click', function () {
					tl.seek(13.7);
				});

				//引数は.to(セレクタ,アニメーション秒数,プロパティ,タイムラインの何秒で動かし始めるか)
				$(window).on('load', function () {
					if (wW > 736) {
						tl.to(".opening-anime__title", 1, {display: 'block',}, 0.1)
							.to(".opening-anime__first-word", 1, {opacity: 1}, 0.1)
							.to(".opening-anime__second-word", 1, {opacity: 1}, 0.1)
							.to(".opening-anime__line-front", 1, {display: "block"}, 2.1)
							.to(".opening-anime__line-back", 1, {display: "block"}, 2.1)
							.fromTo(".opening-anime__image", 8, {scale: (1.5, 1.5),x: "200px"}, {scale: (1.5, 1.5),x: "-100px"}, 4.5)
							.to(".opening-anime__title", 1, {opacity: 0}, 5)
							.to(".opening-anime__image", 1, {opacity: 1}, 5)
							.to(".opening-anime__title", 1, {display: 'none',}, 6.5)
							.to(".opening-anime__image", 1, {opacity: 0}, 8.5)
							.to(".opening-anime", 1, {opacity: 0}, 8.5)
							.to(".opening-anime", 0, {display: "none"}, 10.5)
							.fromTo(".main-visual__bg", 4, { scale: (1.2, 1.2), ease: Power0.easeNone }, { scale: (1, 1), ease: Power0.easeNone }, 8.5)
							.fromTo(".main-visual__building", 4, { scale: (1.1, 1.1), ease: Power1.easeOut }, { scale: (1, 1), ease: Power1.easeOut }, 8.5)
							.fromTo(".main-visual__image", 3, {transform: "scale(1.4, 1.4)"}, {transform: "scale(1, 1)"}, 8.3)
							.to(".main-visual__heading", 1, {opacity: 1}, 12)
							.to(".main-visual__character--01", 1, {opacity: 1}, 12.1)
							.to(".main-visual__character--02", 1, {opacity: 1}, 12.2)
							.to(".main-visual__character--03", 1, {opacity: 1}, 12.3)
							.to(".main-visual__character--04", 1, {opacity: 1}, 12.4)
							.to(".main-visual__character--05", 1, {opacity: 1}, 12.5)
							.to(".main-visual__character--06", 1, {opacity: 1}, 12.6)
							.to(".main-visual__text-line--01", 1, {opacity: 1}, 12.7)
							.to(".main-visual__character--07", 1, {opacity: 1}, 12.7)
							.to(".main-visual__character--08", 1, {opacity: 1}, 12.8)
							.to(".main-visual__character--09", 1, {opacity: 1}, 12.9)
							.to(".main-visual__character--10", 1, {opacity: 1}, 13)
							.to(".main-visual__character--11", 1, {opacity: 1}, 13.1)
							.to(".main-visual__text-line--02", 1, {opacity: 1}, 13.2)
							.to(".main-visual__character--12", 1, {opacity: 1}, 13.2)
							.to(".main-visual__character--13", 1, {opacity: 1}, 13.3)
							.to(".main-visual__character--14", 1, {opacity: 1}, 13.4)
							.to(".main-visual__character--15", 1, {opacity: 1}, 13.5)
							.to(".main-visual__character--16", 1, {opacity: 1}, 13.6)
							.to(".main-visual__character--17", 1, {opacity: 1}, 13.7)
							.to(".main-visual__text-line--03", 1, {opacity: 1}, 13.7);
						tl.eventCallback('onComplete', end);
						function end() {
							$('body').removeClass('no-scroll');
						}
					} else {
						no_scroll();

						tl.to(".opening-anime__title", 1, {display: 'block',}, 0.5)
							.to(".opening-anime__first-word", 1, {opacity: 1}, 1.5)
							.to(".opening-anime__second-word", 1, {opacity: 1}, 2.5)
							.to(".opening-anime__line-front", 1, {display: "block"}, 3.5)
							.to(".opening-anime__line-back", 1, {display: "block"}, 3.5)
							.fromTo(".opening-anime__image", 10, {scale: (1.5, 1.5), x: '50px'}, {scale: (1.5, 1.5), x: '-50px'}, 5.5)
							.to(".opening-anime__title", 1, {opacity: 0}, 6)
							.to(".opening-anime__image", 1, {opacity: 1}, 6)
							.to(".opening-anime__title", 1, {display: 'none',}, 6.5)
							.to(".opening-anime__image", 1, {opacity: 0}, 8.5)
							.to(".opening-anime", 1, {opacity: 0}, 8.5)
							.to(".opening-anime", 1, { display: "none" }, 10.5)
							// .to(".opening-anime", 1, { display: "none" }, 14)
							.fromTo(".main-visual__bg", 4, { scale: (1.2, 1.2), ease: Power0.easeNone }, { scale: (1, 1), ease: Power0.easeNone }, 8.5)
							.fromTo(".main-visual__building", 4, { scale: (1.1, 1.1), ease: Power1.easeOut }, { scale: (1, 1), ease: Power1.easeOut }, 8.5)
							.fromTo(".main-visual__image", 3, {scale: (1.4, 1.4)}, {scale: (1, 1)}, 8.3)
							.to(".main-visual__heading", 1, {opacity: 1}, 12)
							.to(".main-visual__character--01", 1, {opacity: 1}, 12.1)
							.to(".main-visual__character--02", 1, {opacity: 1}, 12.2)
							.to(".main-visual__character--03", 1, {opacity: 1}, 12.3)
							.to(".main-visual__character--04", 1, {opacity: 1}, 12.4)
							.to(".main-visual__character--05", 1, {opacity: 1}, 12.5)
							.to(".main-visual__character--06", 1, {opacity: 1}, 12.6)
							.to(".main-visual__text-line--01", 1, {opacity: 1}, 12.1)
							.to(".main-visual__character--07", 1, {opacity: 1}, 12.7)
							.to(".main-visual__character--08", 1, {opacity: 1}, 12.8)
							.to(".main-visual__character--09", 1, {opacity: 1}, 12.9)
							.to(".main-visual__character--10", 1, {opacity: 1}, 13.5)
							.to(".main-visual__character--11", 1, {opacity: 1}, 13.1)
							.to(".main-visual__text-line--02", 1, {opacity: 1}, 13.5)
							.to(".main-visual__character--12", 1, {opacity: 1}, 13.2)
							.to(".main-visual__character--13", 1, {opacity: 1}, 13.3)
							.to(".main-visual__character--14", 1, {opacity: 1}, 13.4)
							.to(".main-visual__character--15", 1, {opacity: 1}, 13.5)
							.to(".main-visual__character--16", 1, {opacity: 1}, 13.6)
							.to(".main-visual__character--17", 1, {opacity: 1}, 13.7)
							.to(".main-visual__text-line--03", 1, {opacity: 1}, 14);
						tl.eventCallback('onComplete', spEnd);
						function spEnd() {
							$('body').removeClass('no-scroll');
							return_scroll();

						}
					}
						tl2.to('.main-visual__building-image--order_first', 6, { opacity: 0 }, 14)
						.to('.main-visual__building-image--order_second', 2, { opacity: 1 }, 14)
						.to('.main-visual__building-image--order_second', 6, { opacity: 0 }, 16)
						.to('.main-visual__building-image--order_third', 2, { opacity: 1 }, 16)
						.to('.main-visual__building-image--order_third', 6, { opacity: 0 }, 18)
						.to('.main-visual__building-image--order_four', 2, { opacity: 1 }, 18)
						.to('.main-visual__building-image--order_four', 6, { opacity: 0 }, 20)
						.to('.main-visual__building-image--order_first', 2, { opacity: 1 }, 20)
						.to('.main-visual__building-image--order_first', 6, { opacity: 0 }, 22)
						.to('.main-visual__building-image--order_second', 2, { opacity: 1 }, 22)
						.to('.main-visual__building-image--order_second', 6, { opacity: 0 }, 24)
						.to('.main-visual__building-image--order_third', 2, { opacity: 1 }, 24)
						.to('.main-visual__building-image--order_third', 6, { opacity: 0 }, 26)
						.to('.main-visual__building-image--order_four', 2, { opacity: 1 }, 26)
						.to('.main-visual__building-image--order_four', 6, { opacity: 0 }, 28)
						.to('.main-visual__building-image--order_first', 2, { opacity: 1 }, 28)
						.to('.main-visual__building-image--order_first', 6, { opacity: 0 }, 22)
						.to('.main-visual__building-image--order_second', 2, { opacity: 1 }, 22)
						.to('.main-visual__building-image--order_second', 6, { opacity: 0 }, 24)
						.to('.main-visual__building-image--order_third', 2, { opacity: 1 }, 24)
						.to('.main-visual__building-image--order_third', 6, { opacity: 0 }, 26)
						.to('.main-visual__building-image--order_four', 2, { opacity: 1 }, 26)
						.to('.main-visual__building-image--order_four', 6, { opacity: 0 }, 28)
						.to('.main-visual__building-image--order_first', 2, { opacity: 1 }, 28)
						.to('.main-visual__building-image--order_first', 6, { opacity: 0 }, 30)
						.to('.main-visual__building-image--order_second', 2, { opacity: 1 }, 30)
						.to('.main-visual__building-image--order_second', 6, { opacity: 0 }, 32)
						.to('.main-visual__building-image--order_third', 2, { opacity: 1 }, 32)
						.to('.main-visual__building-image--order_third', 6, { opacity: 0 }, 34)
						.to('.main-visual__building-image--order_four', 2, { opacity: 1 }, 34)
						.to('.main-visual__building-image--order_four', 6, { opacity: 0 }, 36)
						.to('.main-visual__building-image--order_first', 2, { opacity: 1 }, 36)
						.to('.main-visual__building-image--order_first', 6, { opacity: 0 }, 38)
						.to('.main-visual__building-image--order_second', 2, { opacity: 1 }, 38)
						.to('.main-visual__building-image--order_second', 6, { opacity: 0 }, 40)
						.to('.main-visual__building-image--order_third', 2, { opacity: 1 }, 40)
						.to('.main-visual__building-image--order_third', 6, { opacity: 0 }, 42)
						.to('.main-visual__building-image--order_four', 2, { opacity: 1 }, 42)
						.to('.main-visual__building-image--order_four', 6, { opacity: 0 }, 44)
						.to('.main-visual__building-image--order_first', 2, { opacity: 1 }, 44)
				});

				$(function () {
					AOS.init({
						offset: 120, // 元のトリガーポイントからのオフセットをpx単位で指定出来ます。
						delay: 0, // 0から3000までの値を50ms(0.05秒。)ずつ指定出来ます。
						duration: 1000, // 0から3000までの値を50ms(0.05秒。)ずつ指定出来ます。
						easing: "ease-out-quart", // イージング
						once: true, // スクロール後、再度アニメーション発動する場合、値をfalseに変更してください。
						anchorPlacement: "top-top" //要素のどの位置でアニメーションをトリガーするかを定義します。top-bottom、top-center、top-topのように2つの単語をハイフンで区切って設定し最初の単語をcenterやbottomにも設定可能です。
					});

					var scrollFunc = function () {
						var $lowIntroduction = $('.low-introduction'),
							$otherContentsItem = $('.other-contents__item'),
							$window = $(window);

						var init = function () {
								bindEvent();
							},

							prepareLowIntroduction = function () {
								var $list = document.querySelectorAll('.low-introduction'),
									$argArr = [].slice.call($list),
									$flagArr = [];

								for (var i = 0; i < $argArr.length; i++) {
									$flagArr.push(false);
								}

								$lowIntroduction.each(function (i) {
									var $this = $(this);
									if ($this.hasClass('aos-animate') && $flagArr[i] === false) {
										$flagArr[i] = true;

										var setOff01 = function (callback) {
												$this.css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff02 = function (callback) {
												$this.find('.low-introduction__visual').css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff03 = function (callback) {
												$this.find('.low-introduction__title').css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff04 = function (callback) {
												$this.find('.low-introduction__row').css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff05 = function () {
												$this.find('.low-introduction__line').css({
													'display': 'block'
												});
											}

										setOff01(setOff02.bind(null, setOff03.bind(null, setOff04.bind(null, setOff05.bind(null)))));
									}
								});
							},

							prepareOtherContents = function () {
								var $list = document.querySelectorAll('.other-contents__item'),
									$argArr = [].slice.call($list),
									$flagArr = [];

								for (var i = 0; i < $argArr.length; i++) {
									$flagArr.push(false);
								}

								$otherContentsItem.each(function (i) {
									var $this = $(this);
									if ($this.hasClass('aos-animate') && $flagArr[i] === false) {
										$flagArr[i] = true;

										var setOff01 = function (callback) {
												$('.other-contents__title').css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff02 = function (callback) {
												$this.css({
													'opacity': 1
												});
												setTimeout(function () {
													callback();
												}, 500);
											},

											setOff03 = function () {
												$('.other-contents').css({
													'background-color': '#f2f3f4'
												});
											}

										setOff01(setOff02.bind(null, setOff03.bind(null)));
									}
								});
							},

							bindEvent = function () {
								$window.on('scroll', function () {
									prepareOtherContents();
									prepareLowIntroduction();
								});
							};

						return {
							init: init
						}
					}

					scrollFunc().init();
				});
				break;

			case 'location':
				var locationTypeWriterAos = new TypeWriterAos(),
					locationTextAnimation = new TextAnimation(),
					locationLowMainVisual = new LowMainVisual();
				locationTypeWriterAos.func();
				locationTextAnimation.func();
				locationLowMainVisual.func();
				break;

			case 'townscape':
				var landscapeTypeWriterAos = new TypeWriterAos(),
					landscapeTextAnimation = new TextAnimation(),
					landscapeLowMainVisual = new LowMainVisual();
				landscapeTypeWriterAos.func();
				landscapeTextAnimation.func();
				landscapeLowMainVisual.func();
				break;

			case 'concept':
				var qualityTypeWriterAos = new TypeWriterAos(),
					qualityTextAnimation = new TextAnimation(),
					qualityLowMainVisual = new LowMainVisual();
				qualityTypeWriterAos.func();
				qualityTextAnimation.func();
				qualityLowMainVisual.func();
				break;

			case 'plan':
				var planTypeWriterAos = new TypeWriterAos(),
					planTextAnimation = new TextAnimation(),
					planLowMainVisual = new LowMainVisual();
				planTypeWriterAos.func();
				planTextAnimation.func();
				planLowMainVisual.func();
				break;

			case 'access':
				var accessFunc = {
					init: function () {
						this.scrollEffect();
						this.routeSearch();
					},

					scrollEffect: function () {
						var accessTypeWriterAos = new TypeWriterAos(),
							accessTextAnimation = new TextAnimation(),
							accessLowMainVisual = new LowMainVisual();
						accessTypeWriterAos.func();
						accessTextAnimation.func();
						accessLowMainVisual.func();
					},

					routeSearch: function () {
						$('.route-search__link').on('click', function () {
							var _obj = $(this).parent();
							start = "祖師ヶ谷大蔵駅",
								goal = $("input", _obj).val() + '駅',
								e_start = encodeURIComponent(start),
								e_goal = encodeURIComponent(goal),
								gmapURL = 'http://maps.google.com/maps?saddr=' + e_start + '&daddr=' + e_goal + '&dirflg=r',
								error_msg_text = '駅名を入力して下さい';

							if (goal == "") {
								swal("", error_msg_text, "error");
							} else {
								window.open(gmapURL, '_blank');
								ga('send', {
									'hitType': 'event',
									// 'eventCategory': 'rootSearch-mst',
									'eventCategory': 'rootSearch-Pichikawa-marks',
									'eventAction': 'click',
									'eventLabel': goal
								});
							}
							return false;
						});
					}
				}

				accessFunc.init();
				break;

			case 'equipment':
				var equipmentTypeWriterAos = new TypeWriterAos(),
					equipmentTextAnimation = new TextAnimation(),
					equipmentLowMainVisual = new LowMainVisual();
				equipmentTypeWriterAos.func();
				equipmentTextAnimation.func();
				equipmentLowMainVisual.func();
				break;

			case 'map':
				var mapTypeWriterAos = new TypeWriterAos(),
					mapTextAnimation = new TextAnimation(),
					mapLowMainVisual = new LowMainVisual();
				mapTypeWriterAos.func();
				mapTextAnimation.func();
				mapLowMainVisual.func();
				break;

			default:
				break;
		}
	});
})(jQuery);
