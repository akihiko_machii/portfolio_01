/**
 * 共通化nav情報
 */
var incNav = incNav || {};
incNav.onArray = [ // 上が優先。
	{dir : '_template', index : null, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'location', index : 1, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'townscape', index : 2, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'concept', index : 3, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'roomplan', index : 4, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'access', index : 5, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'equipment', index : 6, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'modelhouse', index : 7, layer : 1, new : false, off: true, next : '', back : ''},
	{dir : 'outline', index : 8, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'map', index : 9, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'limited', index : 10, layer : 1, new : false, off: false, next : '', back : ''}
];

/**
 * Topic CV Package.
 */
var tcp = {};
tcp.onChoice = null;
tcp.onArray = {};
tcp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(tcp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) tcp.onChoice = incNav.onArray[i];
		}
		tcp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(tcp.onChoice == null) tcp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(tcp.createTag());


};
tcp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < tcp.onChoice.layer; i++){
			layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(tcp.onChoice.index == 0) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(tcp.onArray._template.off) result += 'off '; off処理
	// next back
	// if(tcp.onChoice.back != '') result += '<li class="back"><a href="../' + tcp.onChoice.back + '/index.html">BACK</a></li>';
	// if(tcp.onChoice.next != '') result += '<li class="next"><a href="../' + tcp.onChoice.next + '/index.html">NEXT</a></li>';


	result += '<nav class="list-property-control-wrap only-sp">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item contact">';
	result += '<a href="tel:" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon contact"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="お問い合わせ"></i> <span class="list-property-control-text">お問い合わせ</span> </a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="' + layer + 'map/index.html" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon localmap"><img src="https://www.proud-web.jp/general/img/ico_tel_01_sp.png" class="only-sp" alt="現地案内図"></i>';
	result += '<span class="list-property-control-text">現地案内図</span>';
	result += '</a>';
	result += '</li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="" target="_blank" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"><img src="https://www.proud-web.jp/general/img/ico_book_01_sp.png" class="only-sp" alt="資料請求"></i><span class="list-property-control-text">資料請求</span> </a>';
	result += '</li>';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="{$RESERVE_URL$}" target="_blank" class="list-property-control-link list-property-control-link-green">';
	// result += '<i class="list-property-control-icon reservation01"><img src="https://www.proud-web.jp/general/img/ico_reservation_01_sp.png" class="only-sp" alt="来場予約"></i> <span class="list-property-control-text">来場予約</span> </a>';
	// result += '</li>';
	// result += '<!-- ボタン数は最大5つまでなのでコメントアウトで調整してください。';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="{$SECONDRESERVE_URL$}" target="_blank" class="list-property-control-link list-property-control-link-green">';
	// result += '<i class="list-property-control-icon reservation02"><img src="https://www.proud-web.jp/general/img/ico_reservation_02_sp.png" class="only-sp" alt="再来場予約"></i> <span class="list-property-control-text">再来場予約</span> </a>';
	// result += '</li>-->';
	// result += '<li class="list-property-control-item">';
	// result += '<a href="' + layer + 'limited/index.html" class="list-property-control-link list-property-control-link-wine">';
	// result += '<i class="list-property-control-icon request"><img src="https://www.proud-web.jp/general/img/ico_request_01_sp.png" class="only-sp" alt="物件エントリー者様限定サイト"></i> <span class="list-property-control-text">物件エントリー者様限定サイト</span> </a>';
	// result += '</li>';
	result += '</ul>';
	result += '</nav>';
	// result += '<div class="box-bukken box-bukken-type01">';
	// result += '<div class="box-bukken-inner">';
	// result += '<figure class="figure-bukken"> <img src="https://www.proud-web.jp/general/img/home/img_dummy_bukken_01_pc.jpg" class="only-pc" alt="dummy"> <img src="https://www.proud-web.jp/general/img/home/img_dummy_bukken_01_sp.jpg" class="only-sp" alt="dummy"> </figure>';
	// result += '</div>';
	// result += '<!-- /box-bukken-inner -->';
	// result += '</div>';
	// result += '<!-- /box-bukken -->';
	// result += '<div class="box-bukken box-bukken-blue">';
	// result += '<div class="box-bukken-inner">';
	// result += '<ul class="list-bukken-button">';
	// result += '<li class="list-bukken-button-item">';
	// result += '<p class="btn-bukken-wrap"><a href="{$OUTLINE_URL$}" target="_blank" class="btn-bukken">物件概要</a></p>';
	// result += '</li>';
	// result += '<li class="list-bukken-button-item">';
	// result += '<p class="btn-bukken-wrap"><a href="' + layer + 'map/index.html" class="btn-bukken">現地案内図</a></p>';
	// result += '</li>';
	// result += '</ul>';
	// result += '<div class="box-bukken-announcement">';
	// result += '<p class="box-bukken-announcement-text">お知らせとしての文言が入ります。お知らせとしての文言が入ります。お知らせとしての文言が入ります。</p>';
	// result += '<p class="btn-bukken-wrap"><a href="#" class="btn-bukken">アクションがはいります</a></p>';
	// result += '</div>';
	// result += '<!-- /box-bukken-announcement -->';
	// result += '</div>';
	// result += '<!-- /box-bukken-inner -->';
	// result += '</div>';
	// result += '<!-- /box-bukken -->';
	// result += '<div class="box-bukken box-bukken-type01">';
	// result += '<div class="box-bukken-inner">';
	// result += '<figure class="figure-bukken"> <img src="https://www.proud-web.jp/general/img/home/img_dummy_bukken_01_pc.jpg" class="only-pc" alt="dummy"> <img src="https://www.proud-web.jp/general/img/home/img_dummy_bukken_01_sp.jpg" class="only-sp" alt="dummy"> </figure>';
	// result += '</div>';
	// result += '<!-- /box-bukken-inner -->';
	// result += '</div>';
	// result += '<!-- /box-bukken -->';

	result += '<div class="ftrCVarea">';
	result += '<div class="ftrCVarea--inner">';
	result += '<div class="wrap">';
	result += '<p class="ftrCVarea--ttl en">CONTACT</p>';
	result += '<p class="ftrCVarea__text">未公開間取りなど最新情報を<br class="sp">お届け致します。</p>';
	result += '<ul class="ftrCVarea--cvBtn">';
	result += '<li class="ftrCVarea--cvBtn__request"><a href="" target="_blank">資料請求はこちら</a></li>';
	// result += '<li class="ftrCVarea--cvBtn__reserve"><a href="">来場予約はこちら</a></li>';
	result += '</ul><!-- /ftrCVarea--cvBtn -->';
	result += '</div><!-- /wrap -->';
	result += '</div><!-- /ftrCVarea--inner -->';
	result += '</div><!-- /ftrCVarea -->';

	// 相互リンクバナー
	if (tcp.onChoice.index == 0) {
		result += '<div class="mutual-link">';
		result += '<div class="wrap">';
		result += '<p class="mutual-link__heading">世田谷エリアで<br class="sp">「プラウドシーズン」が続々と誕生</p>';
		result += '<ul class="mutual-link__bnr-list">';
		result += '<li class="mutual-link__bnr-item"><a class="mutual-link__bnr-link" href="https://www.proud-web.jp/house/futakotamagawa/" target="_blank"><img class="mutual-link__bnr-image" src="' + layer + 'imgs/bnr-futagotamagawa.jpg" alt="プラウドシーズン二子玉川"></a></li>';
		result += '<li class="mutual-link__bnr-item"><a class="mutual-link__bnr-link" href="https://www.proud-web.jp/house/jiyugaoka21/" target="_blank"><img class="mutual-link__bnr-image" src="' + layer + 'imgs/bnr-jiyugaoka.jpg" alt="プラウドシーズン自由が丘トレサージュ"></a></li>';
		result += '</ul>';
		result += '</div>';
		result += '</div>';
	}

	result += '<nav class="ftrNav local-nav">';
	result += '<div class="ftrNav-inner ftrNav-inner-first">';
	result += '<ul class="ftrNav-list">';
	result += '<li class="ftrNav-item"><a href="' + layer + 'index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 0) result += 'is-current ';
	result += 'js-match-height">トップ</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.location.off) result += 'is-off ';
	result += '"><a href="' + layer + 'location/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 1) result += 'is-current ';
	result += ' js-match-height">ロケーション</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.townscape.off) result += 'is-off ';
	result += '"><a href="' + layer + 'townscape/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 2) result += 'is-current ';
	result += ' js-match-height">街並み</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.concept.off) result += 'is-off ';
	result += '"><a href="' + layer + 'concept/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 3) result += 'is-current ';
	result += ' js-match-height">邸宅品質</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.roomplan.off) result += 'is-off ';
	result += '"><a href="' + layer + 'roomplan/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 4) result += 'is-current ';
	result += ' js-match-height">プラン</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.access.off) result += 'is-off ';
	result += '"><a href="' + layer + 'access/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 5) result += 'is-current ';
	result += ' js-match-height">アクセス</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.equipment.off) result += 'is-off ';
	result += '"><a href="' + layer + 'equipment/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 6) result += 'is-current ';
	result += ' js-match-height">設備・仕様</a></li>';
	result += '<li class="ftrNav-item ';
	if(tcp.onArray.modelhouse.off) result += 'is-off ';
	result += '"><a href="' + layer + 'modelhouse/index.html" class="ftrNav-link ';
	if(tcp.onChoice.index == 7) result += 'is-current ';
	result += ' js-match-height">モデルハウス</a></li>';
	result += '</ul>';
	result += '</div>';
	result += '</nav>';

	result += '<div class="ftrCaption">';
	result += '<div class="wrap">';
	result += '<p class="caption">';
	if (tcp.onChoice.index == 0) result += '※1:総戸数21戸中、14戸が4LDK<br>※2:高性能断熱材や高断熱サッシなどの採用により、ZEHレベルの断熱性能である外皮平均熱貫流率 UA値0.60W/m²K以下を実現。<br>※掲載の完成予想図は計画段階の図面を基に描いたもので実際とは異なります。また、今後変更になる場合があります。なお、外観の細部・設備機器・配管・照明機器等および周辺建物・電柱・架線・標識等は一部省略又は簡略化しております。タイルや各種部材につきましては、実物と質感・色等の見え方が異なる場合があります。植栽は特定の季節の状況を表現したものでは無く、竣工時には完成予想図程度には成長しておりません。<br>※掲載の徒歩分数は80mを1分として算出（端数切り上げ）したものです。<br>※新宿：（通勤時）小田急線利用、「経堂」駅で小田急線通勤準急乗換、「代々木上原」駅で小田急線快速急行乗換（日中）小田急線準急利用、「代々木上原」駅で小田急線急行乗換 渋谷：（通勤時）小田急線利用、「経堂」駅で小田急線通勤準急乗換、「下北沢」駅で京王井の頭線急行乗換（日中）小田急線準急利用、「下北沢」駅で京王井の頭線急行乗換<br>※掲載の電車所要時間は日中平常時のもので、乗換え・待ち時間は含まれておりません。また、時間帯により異なります。（ジョルダン「乗換案内・時刻表・運行情報サービス」2018年11月版調べ）<br>※掲載の電車所要時間の内容は2018年11月時点の情報であり、今後変更になる可能性があります。<br>※掲載の参考写真は当社分譲済施工例です。';
	//location
	if (tcp.onChoice.index == 1) result += '※掲載の写真は2018年7月に撮影したものです。周辺環境は変わる可能性があり、将来にわたり保証されているものではありません。<br>※掲載の徒歩分数は80mを1分として算出（端数切り上げ）したものです。 <br>※掲載の駅周辺概念図は地図を基に簡略化して描き起こしたもので位置、距離等は実際とは異なります。また、周辺の建物・道路等は、省略・簡略化しております。';
	//landscape
	if (tcp.onChoice.index == 2) result += '※掲載の完成予想図は計画段階の図面を基に描いたもので実際とは異なります。また、今後変更になる場合があります。なお、外観の細部・設備機器・配管・照明機器等および周辺建物・電柱・架線・標識等は一部省略又は簡略化しております。タイルや各種部材につきましては、実物と質感・色等の見え方が異なる場合があります。植栽は特定の季節の状況を表現したものでは無く、竣工時には完成予想図程度には成長しておりません。<br>※掲載の敷地配置イラストは計画段階の図面を基に描いたもので実際とは異なります。また、今後変更になる場合があります。掲載の図面は計画段階のものであり、変更になる場合があります。<br>※掲載の参考写真は当社分譲済施工例です。<br>※植栽の写真は全て参考写真です。植栽の種類は計画段階のもので、今後変更となる場合があります。';
	//concept
	if (tcp.onChoice.index == 3) result += '※1:高性能断熱材や高断熱サッシなどの採用により、ZEHレベルの断熱性能である外皮平均熱貫流率 UA値0.60W/m²K以下を実現。<br>※2:セコムとの契約後の開始となります。<br>※3:ご使用状況などによりメリット額は異なります。機器費・設置工事費は除く。<br>※4:スマートフォンやルーター、LANケーブル等の必要機器・設備はお客さまご自身でご用意いただく必要があります。また、ルーターの機種や設定、インターネットの通信環境によっては、サービスをご利用いただけない場合があります。<br>※掲載の参考写真は当社分譲済施工例です。また、掲載の設備写真はすべて参考写真で、実際とは異なります。';
	//plan
	var $pathName = location.pathname;
	if (tcp.onChoice.index == 4) {
		// stage
		if ($pathName.match('STAGE')) {
			result += '※掲載の参考写真は当社分譲済施工例です。';
		// index
		} else {
			result += '※1:総戸数21戸中、14戸が4LDK<br>※2:高性能断熱材や高断熱サッシなどの採用により、ZEHレベルの断熱性能である外皮平均熱貫流率 UA値0.60W/m²K以下を実現。<br>※掲載の参考写真は当社分譲済施工例です。<br>※掲載の完成予想図は計画段階の図面を基に描いたもので実際とは異なります。また、今後変更になる場合があります。なお、外観の細部・設備機器・配管・照明機器等および周辺建物・電柱・架線・標識等は一部省略又は簡略化しております。タイルや各種部材につきましては、実物と質感・色等の見え方が異なる場合があります。植栽は特定の季節の状況を表現したものでは無く、竣工時には完成予想図程度には成長しておりません。<br>※掲載の図面は計画段階のものであり、変更になる場合があります。';
		}
	}
	//access
	if (tcp.onChoice.index == 5) result += '※掲載の路線図は一部路線・駅等を抜粋して表記しています。<br>※掲載の電車所要時間は日中平常時のもので、乗換え・待ち時間は含まれておりません。また、時間帯により異なります。（ジョルダン「乗換案内・時刻表・運行情報サービス」2018年11月版調べ）<br>※掲載の電車所要時間の内容は2018年11月時点の情報であり、今後変更になる可能性があります。';
	//equipment
	if (tcp.onChoice.index == 6) result += '※掲載の参考写真は当社分譲済施工例です。また、掲載の設備写真はすべて参考写真で、実際とは異なります。';
	//modelhouse
	if(tcp.onChoice.index == 7) result += 'キャプションが入りますキャプションが入りますキャプションが入りますキャプションが入りますキャプションが入ります';
	//map
	if (tcp.onChoice.index == 9) result += '※掲載の写真は2018年7月に撮影したものです。周辺環境は変わる可能性があり、将来にわたり保証されているものではありません。<br>※掲載の徒歩分数は80mを1分として算出（端数切り上げ）したものです。';
	result += '</p>';
	result += '</div><!-- /wrap -->';
	result += '</div><!-- /ftrCaption -->';

	result += '<div class="box-bukken-contact">';
	result += '<div class="box-bukken-contact-inner">';
	result += '<ul class="list-bukken-contact">';
	result += '<li class="list-bukken-contact-item favorite"> <a href="#" class="list-bukken-contact-link btn-basic-favorite js-fav"><span class="btn-basic-favorite-icon">お気に入り</span></a> </li>';
	result += '<li class="list-bukken-contact-item entry">';
	result += '<a href="" target="_blank" class="list-bukken-contact-link list-bukken-contact-link-blue">';
	result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">資料請求</span> </a>';
	result += '</li>';
	// result += '<li class="list-bukken-contact-item reservation01">';
	// result += '<a href="{$RESERVE_URL$}" target="_blank" class="list-bukken-contact-link list-bukken-contact-link-green">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">来場予約</span> </a>';
	// result += '</li>';
	// result += '<li class="list-bukken-contact-item reservation02">';
	// result += '<a href="{$SECONDRESERVE_URL$}" target="_blank" class="list-bukken-contact-link list-bukken-contact-link-green">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">再来場予約</span> </a>';
	// result += '</li>';
	// result += '<li class="list-bukken-contact-item request">';
	// result += '<a href="' + layer + 'limited/index.html" target="_blank" class="list-bukken-contact-link list-bukken-contact-link-wine">';
	// result += '<i class="list-bukken-contact-icon"></i> <span class="list-bukken-contact-text">資料請求者限定サイト</span> </a>';
	// result += '</li>';
	result += '</ul>';
	result += '<p class="box-bukken-contact-title">お問い合わせは<br class="sp">「プラウドシーズン世田谷砧」販売準備室</p>';
	result += '<p class="box-bukken-contact-tel"><a href="tel:" class="js-checktel">{$TEL}</a></p>';
	result += '<p class="box-bukken-contact-text">営業時間／平日 11:00～18:00 土日祝 10:00～18:00';
	result += '<br class="only-sp">定休日／毎週水・木・第二火曜</p>';
	result += '<ul class="box-bukken-contact-list">';
	result += '<li class="box-bukken-contact-list-item"><a href="' + layer + 'outline/index.html" class="box-bukken-contact-list-link">物件概要</a></li>';
	result += '<li class="box-bukken-contact-list-item"><a href="' + layer + 'map/index.html" class="box-bukken-contact-list-link">現地案内図</a></li>';
	result += '</ul>';
	result += '</div>';
	result += '<!-- /box-bukken-contact-inner -->';
	result += '</div>';
	result += '<!-- /box-bukken-contact -->';
	result += '</div>';
	result += '<!-- /contents-property -->';
	result += '<!----　End Contents　---->';

	return result;
};
tcp.init();
