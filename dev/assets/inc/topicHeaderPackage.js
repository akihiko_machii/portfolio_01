/**
 * 共通化nav情報
 */
var incNav = incNav || {};
incNav.onArray = [ // 上が優先。
	{dir : '_template', index : null, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'location', index : 1, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'townscape', index : 2, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'concept', index : 3, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'roomplan', index : 4, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'access', index : 5, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'equipment', index : 6, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'modelhouse', index : 7, layer : 1, new : false, off: true, next : '', back : ''},
	{dir : 'outline', index : 8, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'map', index : 9, layer : 1, new : false, off: false, next : '', back : ''},
	{dir : 'limited', index : 10, layer : 1, new : false, off: false, next : '', back : ''}
];

/**
 * Topic Header Package.
 */
var thp = {};
thp.onChoice = null;
thp.onArray = {};
thp.init = function(){
	for(var i = 0; i < incNav.onArray.length; i++){
		if(thp.onChoice == null){
			if(location.href.indexOf(incNav.onArray[i].dir) > -1) thp.onChoice = incNav.onArray[i];
		}
		thp.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
	}
	if(thp.onChoice == null) thp.onChoice = {dir : '', index : 0, layer : 0, next : '', back : ''};
	window.document.write(thp.createTag());
};
thp.createTag = function(){
	var result = '';
	var layer = '';
	for(var i = 0; i < thp.onChoice.layer; i++){
		layer += '../';
	}

	// href="' + layer + ' // パス（../）の変数
	// if(thp.onChoice.index == 1) result += 'is-current '; それぞれ固有の処理をさせたい時（番号はindexに基づく
	// if(thp.onArray._template.new) result += 'is-new '; NEWマーク
	// if(thp.onArray._template.off) result += 'is-off '; off処理


	result += '<header class="header-property">';
	result += '<div class="header-property-box">';
	result += '<div class="header-property-top-bg">';
	result += '<div class="header-property-top">';
	result += '<div class="header-property-logo">';
	result += '<a href="' + layer + 'index.html" class="header-property-logo-link">';
	result += '<p class="header-property-logo-title header-property-logo-large"><img src="' + layer + 'assets/imgs/logo.svg" alt="PROUD SEASON プラウドシーズン世田谷砧"></p>';
	result += '</a>';
	result += '</div>';
	result += '<div class="header-property-utility">';
	result += '<ul class="header-property-list01 only-pc">';
	result += '<li class="header-property-list01-item"><a href="' + layer + 'outline/index.html">物件概要</a></li>';
	result += '<li class="header-property-list01-item"><a href="' + layer + 'map/index.html">現地案内図</a></li>';
	result += '</ul>';
	result += '<nav class="list-property-control-wrap only-pc">';
	result += '<ul class="list-property-control">';
	result += '<li class="list-property-control-item list-property-control-item-favorite"> <a href="#" class="header-property-favorite btn-basic-favorite js-fav"><span class="btn-basic-favorite-icon">お気に入り</span></a> </li>';
	result += '<li class="list-property-control-item">';
	result += '<a href="" target="_blank" class="list-property-control-link list-property-control-link-blue">';
	result += '<i class="list-property-control-icon entry"></i><span class="list-property-control-text">資料請求</span></a>';
	result += '</li>';
	result += '</ul>';
	result += '</nav>';
	result += '<div class="header-property-btn-favorite only-sp"> <a href="#" class="header-property-btn-favorite-link js-fav" data-contents-number="1"><span class="header-property-btn-favorite-inner"><span>お気に入り</span></span></a> </div>';
	// result += '<p class="header-property-btn-menu js-property-menu header-brand-menu only-sp">総合メニュー</p>';
	result += '<p class="header-property-btn-menu js-property-menu header-local-menu only-sp">物件メニュー</p>';
	result += '</div>';
	result += '<!-- /header-property-utility -->';
	result += '<div class="header-property-menu">';
	// result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="pc-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += '</div>';
	result += '<!-- /header-property-menu-box -->';
	result += '</div>';
	result += '<!-- /header-property-menu -->';
	result += '</div>';
	result += '<!-- /header-property-top -->';
	result += '</div><!-- /header-property-top-bg -->';
	result += '<nav class="nav-global-property brand-nav">';
	result += '<div class="header-property-menu">';
	// result += '<p class="header-property-menu-button js-accd-header-button"><span class="header-property-menu-icon">総合メニュー</span></p>';
	result += '<div class="header-property-menu-box js-accd-header-box">';
	result += '<div id="sp-navi-include-container"></div>';
	result += '<p class="header-property-menu-button-close"><a href="#">閉じる</a></p>';
	result += '</div>';
	result += '<!-- /header-property-menu-box -->';
	result += '</div>';
	result += '<!-- /header-property-menu -->';
	result += '</nav>';
	result += '<nav class="nav-global-property local-nav">';
	result += '<div class="nav-global-property-inner nav-global-property-inner-first">';
	result += '<ul class="nav-global-property-list">';
	result += '<li class="nav-global-property-item"><a href="' + layer + 'index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 0) result += 'is-current ';
	result += 'js-match-height">トップ</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.location.new) result += 'is-new ';
	if(thp.onArray.location.off) result += 'is-off ';
	result += '"><a href="' + layer + 'location/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 1) result += 'is-current ';
	result += ' js-match-height">ロケーション</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.townscape.new) result += 'is-new ';
	if(thp.onArray.townscape.off) result += 'is-off ';
	result += '"><a href="' + layer + 'townscape/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 2) result += 'is-current ';
	result += ' js-match-height">街並み</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.concept.new) result += 'is-new ';
	if(thp.onArray.concept.off) result += 'is-off ';
	result += '"><a href="' + layer + 'concept/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 3) result += 'is-current ';
	result += ' js-match-height">邸宅品質</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.roomplan.new) result += 'is-new ';
	if(thp.onArray.roomplan.off) result += 'is-off ';
	result += '"><a href="' + layer + 'roomplan/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 4) result += 'is-current ';
	result += ' js-match-height">プラン</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.access.new) result += 'is-new ';
	if(thp.onArray.access.off) result += 'is-off ';
	result += '"><a href="' + layer + 'access/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 5) result += 'is-current ';
	result += ' js-match-height">アクセス</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.equipment.new) result += 'is-new ';
	if(thp.onArray.equipment.off) result += 'is-off ';
	result += '"><a href="' + layer + 'equipment/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 6) result += 'is-current ';
	result += ' js-match-height">設備・仕様</a></li>';
	result += '<li class="nav-global-property-item ';
	if(thp.onArray.modelhouse.new) result += 'is-new ';
	if(thp.onArray.modelhouse.off) result += 'is-off ';
	result += '"><a href="' + layer + 'modelhouse/index.html" class="nav-global-property-link ';
	if(thp.onChoice.index == 10) result += 'is-current ';
	result += ' js-match-height">モデルハウス</a></li>';
result += '<li class="nav-global-property-item only-sp ';
result += '"><a href="' + layer + 'outline/index.html" class="nav-global-property-link ';
result += ' js-match-height">物件概要</a></li>';
	result += '</ul>';
	result += '<p class="header-property-menu-button-close"><a href="">閉じる</a></p>';
	result += '</div>';
	// result += '<div class="nav-global-property-inner nav-global-property-inner-second">';
	// result += '<ul class="nav-global-property-list">';
	// result += '<li class="nav-global-property-item only-pc ';
	// if(thp.onArray.modelroom.new) result += 'is-new ';
	// if(thp.onArray.modelroom.off) result += 'is-off ';
	// result += '"><a href="' + layer + 'modelroom/index.html" class="nav-global-property-link ';
	// if(thp.onChoice.index == 9) result += 'is-current ';
	// result += ' js-match-height">モデルルーム</a></li>';
	// result += '<li class="nav-global-property-item ';
	// if(thp.onArray.order.new) result += 'is-new ';
	// if(thp.onArray.order.off) result += 'is-off ';
	// result += '"><a href="' + layer + 'order/index.html" class="nav-global-property-link ';
	// if(thp.onChoice.index == 11) result += 'is-current ';
	// result += ' js-match-height">オーダーメイド</a></li>';
	// result += '<li class="nav-global-property-item ';
	// if(thp.onArray.equipment.new) result += 'is-new ';
	// if(thp.onArray.equipment.off) result += 'is-off ';
	// result += '"><a href="' + layer + 'equipment/index.html" class="nav-global-property-link ';
	// if(thp.onChoice.index == 12) result += 'is-current ';
	// result += ' js-match-height">設備・仕様</a></li>';
	// result += '<li class="nav-global-property-item ';
	// if(thp.onArray.result.new) result += 'is-new ';
	// if(thp.onArray.result.off) result += 'is-off ';
	// result += '"><a href="' + layer + 'result/index.html" class="nav-global-property-link ';
	// if(thp.onChoice.index == 13) result += 'is-current ';
	// result += ' js-match-height">実績</a></li>';
	// result += '<li class="nav-global-property-item only-sp ';
	// result += '"><a href="' + layer + 'outline/index.html" class="nav-global-property-link ';
	// result += ' js-match-height">物件概要</a></li>';
	// result += '</ul>';
	// result += '</div>';
	result += '</nav>';
	result += '</div>';
	result += '<!-- /header-property -->';
	result += '</header>';
	result += '<!-- /header-property -->';

	result += '<!----　Start Contents　---->';
	result += '<div class="contents-property">';
	result += '<h1 class="ttl-property-main">【PROUD 公式ホームページ】プラウドシーズン世田谷砧 | 小田急小田原線「祖師ヶ谷大蔵」駅徒歩10分 新築一戸建て｜野村不動産の新築一戸建て</h1>';

	result += '<nav class="nav" id="accessibilityNav">';
	result += '<ul>';
	result += '<li><a href="#mainContent">コンテンツへ</a></li>';
	result += '<li><a href="#gNav">ナビゲーションへ</a></li>';
	result += '</ul>';
	result += '</nav><!-- / #accessibilityNav -->';

	return result;
};
thp.init();
